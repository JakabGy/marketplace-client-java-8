package com.atlassian.marketplace.client.model;

/**
 * Summary information about user reviews for an add-on.
 * @since 2.0.0
 */
public final class AddonReviewsSummary
{
    Float averageStars;
    Integer count;

    /**
     * The average rating of the add-on, from 0 to 4 (will be zero if it has never been rated).
     */
    public float getAverageStars()
    {
        return averageStars;
    }
    
    /**
     * The number of times the add-on has been reviewed.
     */
    public int getCount()
    {
        return count;
    }
}
