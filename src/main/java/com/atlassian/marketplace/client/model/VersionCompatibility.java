package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.HostingType;
import com.google.common.base.Predicate;
import io.atlassian.fugue.Option;
import io.atlassian.fugue.Pair;

import io.atlassian.fugue.Option;
import io.atlassian.fugue.Pair;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;
import static io.atlassian.fugue.Pair.pair;
import static java.util.Objects.requireNonNull;

/**
 * A range of application versions compatible with an add-on version or product version.
 * @since 2.0.0
 */
@ParametersAreNonnullByDefault
public final class VersionCompatibility
{
    private final ApplicationKey application;
    private final CompatibilityHosting hosting;
    
    VersionCompatibility(ApplicationKey application, CompatibilityHosting hosting)
    {
        this.application = application;
        this.hosting = hosting;
    }

    CompatibilityHosting getHosting() {
        return hosting;
    }

    static final class CompatibilityHosting
    {
        @Nonnull
        final Option<CompatibilityHostingBounds> server;
        @Nonnull
        final Option<CompatibilityHostingBounds> dataCenter;
        final Option<Boolean> cloud;

        CompatibilityHosting(Option<CompatibilityHostingBounds> server, Option<CompatibilityHostingBounds> dataCenter, Option<Boolean> cloud)
        {
            this.server = requireNonNull(server);
            this.dataCenter = requireNonNull(dataCenter);
            this.cloud = requireNonNull(cloud);
        }

        boolean isServerCompatible()
        {
            return server.isDefined();
        }

        boolean isDataCenterCompatible() { return dataCenter.isDefined(); }
        
        boolean isCloudCompatible()
        {
            return cloud.getOrElse(false);
        }

        @Nonnull
        Option<CompatibilityHostingBounds> getServer() {
            return server;
        }

        @Nonnull
        Option<CompatibilityHostingBounds> getDataCenter() {
            return dataCenter;
        }

        Option<Boolean> getCloud() {
            return cloud;
        }
    }
    
    static final class CompatibilityHostingBounds
    {
        @Nonnull
        final VersionPoint min;
        @Nonnull
        final VersionPoint max;
        
        CompatibilityHostingBounds(VersionPoint min, VersionPoint max)
        {
            this.min = min;
            this.max = max;
        }
    }
    
    static final class VersionPoint
    {
        final int build;
        final Option<String> version;
        
        VersionPoint(int build, Option<String> version)
        {
            this.build = build;
            this.version = version;
        }
    }

    /**
     * The application key.
     */
    public ApplicationKey getApplication()
    {
        return application;
    }

    /**
     * Returns true if the version is compatible with Atlassian Cloud instances.
     */
    public boolean isCloudCompatible()
    {
        return hosting.isCloudCompatible();
    }

    /**
     * Returns true if the version is compatible with Atlassian Server instances.
     */
    public boolean isServerCompatible()
    {
        return hosting.isServerCompatible();
    }

    /**
     * Returns true if the version is compatible with Atlassian Datacenter instances.
     */
    public boolean isDataCenterCompatible()
    {
        return hosting.isDataCenterCompatible();
    }

    /**
     * The minimum and maximum application build numbers with which the version is compatible
     * in a Server installation. 
     */
    public Option<Pair<Integer, Integer>> getServerBuildRange()
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return some(pair(b.min.build, b.max.build));
        }
        return none();
    }

    public boolean isInServerBuildRange(int build)
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return build >= b.min.build && build <= b.max.build;
        }
        return false;
    }

    public boolean isInDataCenterBuildRange(int build)
    {
        for (CompatibilityHostingBounds b: hosting.dataCenter)
        {
            return build >= b.min.build && build <= b.max.build;
        }
        return false;
    }

    public Option<Integer> getServerMinBuild()
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return some(b.min.build);
        }
        return none();
    }

    public Option<Integer> getServerMaxBuild()
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return some(b.max.build);
        }
        return none();
    }

    public Option<Integer> getDataCenterMinBuild()
    {
        return hosting.dataCenter.map(b -> b.min.build);
    }

    public Option<Integer> getDataCenterMaxBuild()
    {
        return hosting.dataCenter.map(b -> b.max.build);
    }

    public boolean isCompatibleWith(Predicate<ApplicationKey> applicationCriteria,
                                    HostingType hostingType, int build) {
        return applicationCriteria.apply(this.getApplication()) &&
                ((hostingType == HostingType.CLOUD && hosting.isCloudCompatible()) ||
                        (hostingType == HostingType.SERVER && hosting.isServerCompatible() &&
                                isInServerBuildRange(build)) ||
                        (hostingType == HostingType.DATA_CENTER && hosting.isDataCenterCompatible() &&
                                isInDataCenterBuildRange(build))
                );
    }

    public static Predicate<VersionCompatibility> compatibleWith(final Predicate<ApplicationKey> applicationCriteria,
        final HostingType hostingType, final int build)
    {
        return c -> c.isCompatibleWith(applicationCriteria, hostingType, build);
    }
}
