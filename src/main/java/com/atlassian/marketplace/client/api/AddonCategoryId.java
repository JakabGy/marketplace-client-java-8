package com.atlassian.marketplace.client.api;

import java.net.URI;

/**
 * Simple value wrapper for a resource URI when it is used to identify an add-on category.
 * @since 2.0.0
 */
public final class AddonCategoryId extends ResourceId
{
    private AddonCategoryId(URI uri)
    {
        super(uri);
    }
    
    public static AddonCategoryId fromUri(URI uri)
    {
        return new AddonCategoryId(uri);
    }
}
